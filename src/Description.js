import React, { Component } from 'react';
import {Collapse} from 'react-collapse';
import {presets} from 'react-motion';
import './App.css';

class Description extends Component {
    constructor(props) {
        super(props);
        this.seeMore = this.seeMore.bind(this);
        this.state = {
            buttonText: "See More",
            isOpen: false
        };
    }

    seeMore(e){
        e.preventDefault();
        if (this.state.buttonText === "See More"){
            this.setState({isOpen: !this.state.isOpen, buttonText: "See Less"});
        }
        else {
            this.setState({isOpen: !this.state.isOpen, buttonText: "See More"});
        }
        
    }

    render() {
        if (this.props.description === undefined) {
            return (<div className='info-desc'>No description available</div>)
        }
        else {
            return (
                <div>
                    <Collapse springConfig={presets.gentle} isOpened={this.state.isOpen} className='info-desc'>{this.props.description}</Collapse>        
                    <button className="see-more" onClick={this.seeMore}>{this.state.buttonText}</button>            
                </div>
            )
        }
    }
}

export default Description;